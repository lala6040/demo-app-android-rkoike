package com.example.demoapp

import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.widget.MediaController
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_video.*


class VideoActivity: AppCompatActivity() {

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
        supportActionBar?.title = "Play Video"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        Handler(mainLooper).postDelayed({
            val moviePath = Uri.parse("android.resource://" + packageName + "/" + R.raw.neko)
            videoView.setVideoURI(moviePath)

            videoView.setOnPreparedListener {
                videoView.start()

                videoView.setMediaController(MediaController(this))
            }

            videoView.setOnCompletionListener {
                finish()
            }
        }, 200)


    }
}


