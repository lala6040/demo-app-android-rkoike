package com.example.recyclerview_test

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.demoapp.R


class MyAdapter(
    private val myDataset: ArrayList<String>,
    private val onClickPicker: () ->Unit,
    private val onClickMap: () -> Unit,
    private val onClickVideo: () -> Unit,
    private val onClickWebView: () -> Unit,
    private val onClickViewPager: () -> Unit,
    private val onClickForm: () -> Unit) : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    class MyViewHolder(val textView: TextView) : RecyclerView.ViewHolder(textView)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {

        val textView = LayoutInflater.from(parent.context)
            .inflate(R.layout.my_text_view, parent, false) as TextView

        return MyViewHolder(textView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.textView.text = myDataset[position]

        if (position == 0 ) {
            holder.textView.setOnClickListener {
                onClickPicker()
            }
        } else if (position == 1) {
            holder.textView.setOnClickListener {
                onClickMap()
            }
        } else if (position == 2) {
            holder.textView.setOnClickListener {
                onClickVideo()
            }
        } else if (position == 3) {
            holder.textView.setOnClickListener {
                onClickWebView()
            }
        } else if (position == 4) {
            holder.textView.setOnClickListener {
                onClickViewPager()
            }
        } else if (position == 5) {
            holder.textView.setOnClickListener {
                onClickForm()
            }
        }
    }

    override fun getItemCount() = myDataset.size
}