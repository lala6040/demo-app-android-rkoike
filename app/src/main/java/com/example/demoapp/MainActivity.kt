package com.example.demoapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview_test.MyAdapter

class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    var myDataset: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addlist()

        viewManager = LinearLayoutManager(this)
        viewAdapter = MyAdapter(myDataset,  ::onClickPicker,
                                            ::onClickMap,
                                            ::onClickVideo,
                                            ::onClickWebView,
                                            ::onClickViewPager,
                                            ::onClickForm)


        recyclerView = findViewById<RecyclerView>(R.id.my_recycler_view).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter
        }
    }

    fun onClickPicker() {
        startActivity(Intent(this@MainActivity, PickerActivity::class.java))
    }
    fun onClickMap() {
        startActivity(Intent(this@MainActivity, MapsActivity::class.java))
    }
    fun onClickVideo() {
        startActivity(Intent(this@MainActivity, VideoActivity::class.java))
    }
    fun onClickWebView() {
        startActivity(Intent(this@MainActivity, WebViewActivity::class.java))
    }
    fun onClickViewPager() {
        startActivity(Intent(this@MainActivity, ViewPagerActivity::class.java))
    }
    fun onClickForm() {
        startActivity(Intent(this@MainActivity, FormActivity::class.java))
    }

    fun addlist() {
        myDataset.add("Picker")
        myDataset.add("Map")
        myDataset.add("Video")
        myDataset.add("Web View")
        myDataset.add("View Pager")
        myDataset.add("Form")
    }
}

