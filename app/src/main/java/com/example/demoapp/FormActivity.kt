package com.example.demoapp

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_form.*
import java.time.LocalDate
import java.time.Period



class FormActivity: AppCompatActivity() {

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private val spinnerYearItems = arrayListOf("")
    private val spinnerMonthItems = arrayListOf("")
    private val spinnerDateItems = arrayListOf("")
    var gender: String = ""
    var year: String = ""
    var month: String = ""
    var date: String = ""
    var inter_box = ""
    var magazin_box = ""
    var friend_box = ""
    var seminer_box = ""
    var other_box = ""

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)
        supportActionBar?.title = "アンケート"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val checkBox1 = findViewById<CheckBox>(R.id.checkBox1)
        val checkBox2 = findViewById<CheckBox>(R.id.checkBox2)
        val checkBox3 = findViewById<CheckBox>(R.id.checkBox3)
        val checkBox4 = findViewById<CheckBox>(R.id.checkBox4)
        val checkBox5 = findViewById<CheckBox>(R.id.checkBox5)

        //性別
        val radioGroup: RadioGroup = findViewById(R.id.radiogroup)
        radioGroup.setOnCheckedChangeListener {_, checkedId: Int ->
            when(checkedId) {
                R.id.radio_man -> gender = "男性"
                R.id.radio_woman -> gender = "女性"
                R.id.radio_other -> gender = "その他"
            }
        }

        //生年月日
        val yearspinner = findViewById<Spinner>(R.id.spinner_year)
        val yaeradapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerYearItems)
        yaeradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        yearspinner.adapter = yaeradapter
        yearspinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val spinnerParent = parent as Spinner
                year = spinnerParent.selectedItem as String
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        for (i in 1950..2020) {
            spinnerYearItems.add("$i")
        }

        val monthspinner = findViewById<Spinner>(R.id.spinner_month)
        val monthadapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerMonthItems)
        monthadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        monthspinner.adapter = monthadapter
        monthspinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val spinnerParent = parent as Spinner
                month = spinnerParent.selectedItem as String
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
        for (i in 1..12) {
            spinnerMonthItems.add("$i")
        }

        val datespinner = findViewById<Spinner>(R.id.spinner_date)
        val dateadapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerDateItems)
        dateadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        datespinner.adapter = dateadapter
        datespinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val spinnerParent = parent as Spinner
                date = spinnerParent.selectedItem as String
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
        for (i in 1..31) {
            spinnerDateItems.add("$i")
        }

        button.setOnClickListener {
            val intent = Intent(this, ResultFormActivity::class.java)
            val birthday: String = year + "年" + month + "月" + date + "日"
            val today = LocalDate.now()
            val age = Period.between(LocalDate.of(year.toInt(),month.toInt(),date.toInt()), today).years.toString()

            if(checkBox1.isChecked)
                inter_box = "インターネット"
            else
                inter_box = ""

            if(checkBox2.isChecked)
                magazin_box = "雑誌記事"
            else
                magazin_box = ""

            if(checkBox3.isChecked)
                friend_box = "友人・知人から"
            else
                friend_box= ""

            if(checkBox4.isChecked)
                seminer_box = "セミナー"
            else
                seminer_box= ""

            if(checkBox5.isChecked)
                other_box = "その他"
            else
                other_box = ""

            intent.putExtra("Name", editText.text.toString())
            intent.putExtra("Gender", gender)
            intent.putExtra("Birthday", birthday)
            intent.putExtra("Age", age)
            intent.putExtra("Internet", inter_box)
            intent.putExtra("Magazin", magazin_box)
            intent.putExtra("Friend", friend_box)
            intent.putExtra("Seminer", seminer_box)
            intent.putExtra("Other", other_box)

            startActivity(intent)
        }
    }
}
