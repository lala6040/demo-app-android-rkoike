package com.example.demoapp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_result_form.*

class ResultFormActivity : AppCompatActivity() {

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result_form)
        supportActionBar?.title = "アンケート"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val name_text = intent.getStringExtra("Name")
        val gender_text = intent.getStringExtra("Gender")
        val birthday_text = intent.getStringExtra("Birthday")
        val age_text = intent.getStringExtra("Age")
        val internet = intent.getStringExtra("Internet")
        val magazin = intent.getStringExtra("Magazin")
        val friend = intent.getStringExtra("Friend")
        val seminer = intent.getStringExtra("Seminer")
        val other = intent.getStringExtra("Other")

        textView11.text = name_text
        textView12.text = gender_text
        textView13.text = birthday_text
        textView14.text = age_text
        result_inter.text = internet
        result_magazin.text = magazin
        result_friend.text = friend
        result_seminer.text = seminer
        result_other.text = other




        button2.setOnClickListener(){
            val intent = Intent(this@ResultFormActivity, MainActivity::class.java)
            startActivity(intent)
        }
    }
}